# README #

### What is this repository for? ###

This my homepage set up using next.js

### How do I get set up? ###

#### install
```
npm install
```
#### development
```
npm run dev
```
hit localhost:3000
#### deploy and run with a server
```
npm run build && npm run start
```
#### deploy into a static folder /out
```
npm run export
```