import React, {Component} from 'react'
import { push as Menu } from 'react-burger-menu'
import MenuSvg from '../menu.svg'

const MenuStyles = {
    bmBurgerButton: {
        position: 'fixed',
        width: '40px',
        height: '40px',
        right: 10,
        top: 10
    },
    bmBurgerBars: {
        background: '#373a47'
    },
    bmCrossButton: {
        height: '24px',
        width: '24px'
    },
    bmCross: {
        background: 'white'
    },
    bmMenu: {
        background: '#3949AB',
        padding: '1.5em 0.5em 0',
    },
    bmMorphShape: {
        fill: '#3949AB'
    },
    bmItemList: {
        color: '#b8b7ad',
        padding: '0.8em'
    },
    bmOverlay: {
        background: 'rgba(0, 0, 0, 0.3)'
    }
}