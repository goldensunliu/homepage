const webpack = require('webpack')
module.exports = {
  exportPathMap: function() {
    return {
      '/': { page: '/' }
    }
  },
  webpack: (config, {
    // eslint-disable-next-line no-unused-vars
    buildId, dev, isServer, defaultLoaders,
  }) => {
    // Perform customizations to webpack config
    config.plugins.push(new webpack.ProvidePlugin({ FastClick : 'fastclick' }));
    // Important: return the modified config
    return config;
  },
}