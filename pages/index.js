import React, {Component} from 'react'
import { bubble as Menu } from 'react-burger-menu'
import Color from 'color'
import Typist from 'react-typist'

import Head from 'next/head'
import Word from '../word'
import globalStyles from '../globalStyles'
import { load } from "../fireworksBackground";
import SkillChart from '../SkillChart'
import MenuSvg from '../menu.svg'
import colors, {orange} from '../colors'


const backGroundMenuColor = Color(colors.blue).darken(.5).hsl().string();
const MenuStyles = {
    bmBurgerButton: {
        position: 'fixed',
        width: '40px',
        height: '40px',
        left: 10,
        top: 10
    },
    bmBurgerBars: {
        background: backGroundMenuColor
    },
    bmCrossButton: {
        height: '24px',
        width: '24px'
    },
    bmCross: {
        background: 'white'
    },
    bmMenu: {
        background: backGroundMenuColor,
        padding: '1.5em 0.5em 0',
    },
    bmMorphShape: {
        fill: backGroundMenuColor
    },
    bmItemList: {
        height: 'initial',
        color: '#b8b7ad',
        padding: '0.8em'
    },
    bmOverlay: {
        background: 'rgba(0, 0, 0, 0.3)'
    }
}

export default class HomePage extends Component {
    state = { openMenu: false }
    async componentDidMount() {
        Array.from(document.querySelectorAll('.word')).forEach((word) => new Word(word));
        load(document.querySelector('.fireworks'))
        FastClick.attach(document.body)
    }

    renderName() {
        return (
            <div className="word word--swing">
                Sitian
                { /*language=CSS*/ }
                <style jsx>{`
                  .word {
                      margin-top: .5em;
                  }
                    div { margin-bottom: .2em; }
                    @media (max-height: 660px) {
                        div { margin-bottom: .1em; }
                    }
                `}</style>
            </div>
        )
    }
    render() {
        return (
            <div id="outerContainerId">
                <Head>
                    <title>Software Engineer, Sitian Liu, Somerville, MA, USA</title>
                    <link rel="icon" type="image/png" href="/static/favicon-16x16.png"/>
                    <meta name="description" content="Welcome to Sitian Liu's home page! Contact me for collaborations and opportunities"/>
                    <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
                </Head>
                <Menu left isOpen={this.state.openMenu} customBurgerIcon={<MenuSvg/>} width={ 200 } styles={MenuStyles} pageWrapId="pageWrapId" outerContainerId="outerContainerId">
                    <div className="menu-item">
                        <a href="mailto:goldensunliu@gmail.com">Contact Me</a>
                    </div>
                    <a className="menu-item" target="_blank" href="https://github.com/goldensunliu">GitHub</a>
                    <a className="menu-item" target="_blank" href="https://soundforspotify.com">Sound Intelligence App for Spotify</a>
                    <a className="menu-item" target="_blank" href="https://out-bqkxoqnwce.now.sh/">AI Color Palette Picking App</a>
                    <a className="menu-item" target="_blank" href="https://sudoku.sitianliu.com/">Best Sudoku Game Ever</a>
                    <a className="menu-item" target="_blank" href="https://spotify-recommendation.sitianliu.com/">Spotify Music Recommendations App</a>
                    <a className="menu-item" target="_blank" href="https://bitbucket.org/goldensunliu/homepage/overview">Homepage Source Code</a>
                </Menu>
                <div className="container" id="pageWrapId">
                    <canvas className="fireworks" id="background-fireworks"></canvas>
                    {this.renderName()}
                    <div className="title">Veteran software engineer passionate for great designs</div>
                    <SkillChart/>
                    <Typist cursor={{hideWhenDone: true}}>
                        Hey you, welcome to my corner of the internet! <br/>
                        <span className="hmu" onClick={() => { this.setState({ openMenu: true })}}>Hit me up</span> if you need anything!
                    </Typist>
                </div>
                <style global jsx>
                    {globalStyles}
                </style>
                { /*language=CSS*/ }
                <style jsx>{`
                    .container {
                        flex-direction: column;
                        display: flex;
                        justify-content: center;
                        align-items: center;
                    }
                    .title {
                        font-size: 1em;
                        text-align: center;
                    }
                    @media (min-width: 600px) {
                        .container { font-size: 24px; }
                    }
                    @media (max-height: 660px) {
                        .container { font-size: 14px; }
                    }
                    .hmu {
                        color: ${orange};
                        font-weight: bold;
                        font-size: 1.25em;
                        cursor: pointer;
                        border: .1em solid ${orange};
                        border-radius: .25em;
                        padding: .25em;
                    }
                    .menu-item, a {
                        color: white;
                        font-size: 18px;
                        font-weight: bold;
                        padding: 15px 0;
                        text-decoration: none;
                    }
                    .menu-item:not(:last-child) {
                        border-bottom: 1px solid;
                    }
                    .container :global(.Typist) {
                        z-index: 1;
                        line-height: 2em;
                        text-align: center;
                        margin-top: 1.5em;
                        margin-bottom: 1.5em;
                        font-size: 1.1em;
                    }
                    @media (max-height: 660px) {
                        .container :global(.Typist)                    {
                            margin-top: .5em;
                            margin-bottom: .5em;
                        }
                    }
                `}</style>
                <link href="https://fonts.googleapis.com/css?family=Nunito:400,700|Londrina+Outline|Londrina+Solid" rel="stylesheet"/>
            </div>
        )
    }
}