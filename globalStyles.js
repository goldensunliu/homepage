import css from 'styled-jsx/css'
import colors from './colors'
import Color from 'color'

const backGroundMenuColor = Color(colors.blue).darken(.5).hsl().string();
{ /*language=CSS*/ }
const styles = css`
body {
    margin: 0;
    font-family: 'Gill Sans', 'Gill Sans MT', 'Ser­avek', 'Trebuchet MS', sans-serif;
}
.bm-burger-button * {
    fill: ${backGroundMenuColor}
}
.word {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
    margin: 0;
    cursor: default;
    user-select: none;
    -webkit-tap-highlight-color: rgba(0,0,0,0);
}

.fireworks {
    position: absolute;
    top: 0;
    left: 0;
    width: 100vw;
    height: 100vh;
    z-index: -1;
}

.word--swing {
    font-size: 11vw;
    font-family: 'Londrina Solid', serif;
    font-weight: 400;
    text-transform: uppercase;
    color: #0e0e19;
    line-height: 0.725;
    perspective: 800px;
}

.word--swing > span:first-child {
    color: #f44336;
}

.word--swing > span:nth-child(2) {
    color: #9C27B0;
}

.word--swing > span:nth-child(3) {
    color: #4CAF50;
}

.word--swing > span:nth-child(4) {
    color: #FFEB3B;
}

.word--swing > span:nth-child(5) {
    color: #FF9800;
}

.word--swing > span:nth-child(6) {
    color: #9C27B0;
}

.word--swing svg {
    position: absolute;
    top: 50%;
    left: 50%;
    width: 450px;
    height: 510px;
    margin: -255px 0 0 -225px;
    pointer-events: none;
}

.word--swing svg circle {
    fill: currentColor;
    opacity: 0;
    transform-origin: 50% 50%;
}

.word--swing span {
    display: block;
    position: relative;
}

.word--swing span span:first-of-type {
    position: absolute;
}

.word--swing span span:last-of-type {
    transform-style: preserve-3d;
}

@media screen and (max-width: 50em) {
    .word.word--swing {
        font-size: 6em;
    }
}
.Typist .Cursor {
  display: inline-block;
}
.Typist .Cursor--blinking {
  opacity: 1;
  animation: blink 1s linear infinite;
}
@keyframes blink {
  0% {
    opacity: 1;
  }
  50% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
}
`

export default styles